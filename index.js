const connect = require('RabAMQP');

const { updateMoistLevel, getAlertMoistLevel, getResponseMoistLevel } = require('./responseMoist');

const APP = 'moes';
const KEY_MOIST = 'moes.moist';
const KEY_INTENT = 'rabot.apps.intents.moes';
const KEY_RESPONSE  = 'rabot.apps.responses.moes';
const KEY_NOTIFICATION = 'rabot.apps.notifications.moes';

connect({
    appName: APP,
    onConnect: ({assertQueue, sendMessage}) => {
        /**
         * INTENT => RABOT RESPONSE: Listen to intents and send a response
         */
        assertQueue(
            'moes.response',
            KEY_INTENT,
            (message) => {
                const {username, intent} = message;

                sendMessage({
                    utterance: getFeedbackOnIntent(intent),
                    replyTo: message
                }, KEY_RESPONSE, username)
            }
        );

        /**
         * SENSOR => RABOT NOTIFICATION: Subscribe to sensor data and send notifications
         */
        assertQueue(
            'moes.notification',
            KEY_MOIST,
            ({moist}) => {
                updateMoistLevel(moist);
                const alert = getAlertMoistLevel();
                if (alert) sendMessage({
                    utterance: alert
                }, KEY_NOTIFICATION)
            }
        );
    }
});

/**
 * Decide how to reply to the intent
 * @param intent
 */
function getFeedbackOnIntent (intent) {
    switch(intent) {
        case "info":
            return "Your garden info...";
            break;
        case "water":
            return getResponseMoistLevel();
            break;
        default:
            return "I did not get that.. did you mean 'weather' or 'water'";
    }
}